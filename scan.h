
struct array cstring_scan(
	void* ptr,
	int (*next)(void* ptr, char* c),
	char* c, bool append_null);
