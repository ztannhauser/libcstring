
void cstring_print(
	struct array* chars,
	void* ptr,
	void (*next)(void* ptr, char* str, int n),
	bool include_quotes,
	bool ignore_last_char
);
