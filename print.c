#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "libarray.h"
#include "libdebug.h"

void cstring_print(
	struct array* chars,
	void* ptr,
	void (*push)(void* ptr, char* str, int n),
	bool include_quotes,
	bool ignore_last_char
)
{
	if(include_quotes)
	{
		push(ptr, "\"", 1);
	}
	int n = chars->n;
	if(ignore_last_char)
	{
		n--;
	}
	for(int i = 0;i < n;i++)
	{
		char ele = array_index(*chars, i, char);
		switch(ele)
		{
			case '\"': push(ptr, "\\\"", 2); break;
			case '\'': push(ptr, "\\\'", 2); break;
			case '\n': push(ptr, "\\\n", 2); break;
			case '\t': push(ptr, "\\\t", 2); break;
			case '\0': push(ptr, "\\0", 2); break;
			default:
			{
				push(ptr, &ele, 1);
			}
		}
	}
	if(include_quotes)
	{
		push(ptr, "\"", 1);
	}
}

