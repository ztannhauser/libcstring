
#include <stdbool.h>

struct array cstring_scan(
	void* ptr,
	int (*next)(void* ptr, char* c),
	char* c, bool append_null);


void cstring_print(
	struct array* chars,
	void* ptr,
	void (*push)(void* ptr, char* str, int n),
	bool include_quotes,
	bool ignore_last_char
);
