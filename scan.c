#include <assert.h>
#include <stdbool.h>

#define DEBUG 0
#include "libdebug.h"
#include "libarray.h"

struct array cstring_scan(
	void* ptr,
	int (*next)(void* ptr, char* c),
	char* c, bool append_null)
{
	ENTER;
	struct array ret = new_array(char);
	assert(*c == '\"');
	assert(next(ptr, c));
	while(*c != '\"')
	{
		verpvc(*c);
		if(*c == '\\')
		{
			assert(next(ptr, c));
			char cc;
			switch(*c)
			{
				case 't': cc = '\t'; break;
				case 'n': cc = '\n'; break;
				case '\'': cc = '\''; break;
				case '\"': cc = '\"'; break;
				default:
				{
					assert(!"unknown C string escape code!");
				}
			}
			array_push_n(&ret, &cc);
		}
		else
		{
			array_push_n(&ret, c);
		}
		assert(next(ptr, c));
	}
	if(append_null)
	{
		char null = '\0';
		array_push_n(&ret, &null);
	}
	assert(next(ptr, c));
	EXIT;
	return ret;
}
